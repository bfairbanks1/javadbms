import java.sql.*;

public class JDBC {
    private Statement stmt;
    private Connection c;

    public JDBC(String dbName) {
        try {
            Class.forName("SQLiteJDBC");
            c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Brandon\\IdeaProjects\\JavaDatabase\\src\\" + dbName);
            System.out.println("Opened database successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.out.println("Could not connect, try again");
        }

    }
}
