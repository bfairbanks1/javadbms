import java.sql.*;
import java.util.*;
import java.io.*;

public class FileOutputer {
    private String tableName;
    private String fileName;

    public FileOutputer(String fileName, String tableName){
        this.tableName = tableName;
        this.fileName = fileName;
    }

    public void outputQueries(String cmd){
        try {
            PrintWriter out = new PrintWriter(new FileWriter("QueryResults.txt"));
            out.println("Hail/Fog Dates after 2000");
            out.println("Month Day Year");

            try {
                Connection c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Brandon\\IdeaProjects\\JavaDatabase\\src\\weather_stations.db");
                PreparedStatement stmt = c.prepareStatement(cmd);
                ResultSet rs;

                rs = stmt.executeQuery();
                while (rs.next()) {
                    String result = (rs.getString("month") + "  ");
                    result += (rs.getString("day") + "   ");
                    result += rs.getString("year");
                    out.println(result);
                }
                c.close();
            }
            catch (SQLException e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
            }
            out.close();
        }
        catch (IOException e){
            System.out.println("File cannot be written to.");
        }
    }
}
