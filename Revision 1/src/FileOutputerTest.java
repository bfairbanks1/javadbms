import java.io.FileOutputStream;

public class FileOutputerTest {
    public static void main(String args[]){
        FileOutputer fo = new FileOutputer("QueryResults.txt", "STATION_DATA");
        fo.outputQueries("SELECT month, day, year FROM STATION_DATA WHERE fog = 1 AND hail = 1 AND year > 2000");
        System.out.println("Query written to file!");
    }
}
