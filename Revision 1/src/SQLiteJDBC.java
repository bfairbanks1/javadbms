import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class SQLiteJDBC {
    private Scanner scnr = new Scanner(System.in);
    private Connection c;
    private Statement stmt;
    private String dbName = "";
    private ArrayList<String> tableNames = new ArrayList<>();
    private ArrayList<String> schema = new ArrayList<>();

    public SQLiteJDBC(){
        c = null;
        stmt = null;
    }

    public SQLiteJDBC(String dbName){
        c = null;
        stmt = null;
        createDatabase(dbName);
    }

    public void changeDB(String dbName){
        this.dbName = dbName;
    }

    public String getDB(){
        return dbName;
    }

    public void mainMenu(){
        System.out.println("This program acts as a DBMS using Java input (and lacks a user interface).");
        System.out.println("Please choose a number corresponding to the action you would like to take:\n" +
                "1. Create a new table in test database.\n" +
                "2. Insert information into test table.\n" +
                //"3. Create a new database.\n" +
                //"4. Update information in table.\n" +
                //"5. Delete information in table.\n\n" +
                "INPUT: ");
        //}
        int input = Integer.parseInt(scnr.nextLine());
        switch(input) {
            case 1:
                createTableInput();
                break;
            case 2:
                insertInfoInput();
                break;
            case 3:
                //System.out.println("Please enter a name for your new database:\n");
                //createDatabase(scnr.nextLine());
                break;
            case 4:
                break;
            default:
                System.out.println("Invalid entry choice:\n" +
                        "Try again(y or n): ");
                if(scnr.nextLine().equalsIgnoreCase("y")) mainMenu();
                else break;
        }
    }

    public void createDatabase(String dbName){
        String createCMD = ("CREATE TABLE " + dbName);
        try {
            this.c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Brandon\\IdeaProjects\\JavaDatabase\\src\\");
            this.stmt = this.c.createStatement();
            stmt.executeUpdate(createCMD);
            System.out.println("Database " + dbName + "created.");
        }
        catch(Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public void createTableInput(){
        System.out.println("Please enter a name for the table: ");
        String temp = "CREATE TABLE ";

        String tableTest = scnr.nextLine();
        if(checkExistence(tableTest)){
            System.out.println("There's already a table named " + tableTest + ". Please try again:\n\n");
            createTableInput();
        }

        temp += (tableTest + " (");

        System.out.println("Now enter the name of the first column, the data type, and any additional constraints" +
                "(ex: Primary Key, Foreign Key, Not Null, etc.");
        temp += scnr.nextLine();

        while(true){
            String in;
            System.out.println("Please enter the next columns name, followed by the data type, and any additional constraints. \nOtherwise enter \"exit\":");
            in = scnr.nextLine();

            if(in.equalsIgnoreCase("exit")) break;
            else temp += (", " + in);
        }

        temp += ")";
        //System.out.println(temp);
        executeCommand(temp);
    }

    public void insertInfoInput(){
        int i = 0;

        System.out.println("Please select a table corresponding to it's number");
        getTableNames();

        String temp = "INSERT INTO TABLE ";
        String table = tableNames.get(Integer.parseInt(scnr.nextLine()) - 1);
        getColumnNames(table);
        temp += table + "VALUES (";

        System.out.println("Enter first value with name and type ( " + (schema.get(i++)) + "): ");
        temp += scnr.nextLine();


        while(true){
            String in;
            System.out.println("Please enter the next value with name. \nOtherwise enter \"exit\":");
            in = scnr.nextLine();

            if(in.equalsIgnoreCase("exit")) break;
            else temp += (", " + in);
        }
        temp += ")";
        System.out.println(temp);
        //executeCommand(temp);
    }

    /* TO BE IMPLEMENTED LATER
    public void updateInfoInput(){
        String temp = "UPDATE ";
        System.out.println("Please select a table corresponding to it's number");
        getTableNames();
        temp += tableNames.get(Integer.parseInt(scnr.nextLine()) - 1);

    }*/

    public void getTableNames()
    {
        try {
            c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Brandon\\IdeaProjects\\JavaDatabase\\src\\SQLTest.db");
            DatabaseMetaData dbmd = c.getMetaData();
            String[] types = {"TABLE"};
            ResultSet rs = dbmd.getTables(null, null, "%", types);
            int i = 1;
            while (rs.next()) {
                tableNames.add(i - 1, rs.getString("TABLE_NAME"));
                System.out.println(Integer.toString(i) + " " + rs.getString("TABLE_NAME"));
                i++;
            }
            c.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getColumnNames(String tableName)
    {
        try {
            c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Brandon\\IdeaProjects\\JavaDatabase\\src\\SQLTest.db");
            ResultSet rs = stmt.executeQuery("SELECT * FROM " + tableName);
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            for (int i = 1; i <= columnCount; i++ ) {
                schema.add(i - 1, rsmd.getColumnName(i - 1));
                System.out.println(schema.get(i - 1));
            }
            c.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /* OLD METHOD
    public String createSQLString(ArrayList<String> temp, String cmdType){
        cmdType += (temp.get(0) + " (");
        for(int i = 0; i < temp.size() - 2; i++){
            cmdType += (temp.get(i) + ", ");
        }
        cmdType += temp.get(temp.size() - 1) + ")";
        System.out.println(cmdType);
        return cmdType;
    }*/

    public boolean checkExistence(String toTest){
        for(int i = 0; i < tableNames.size(); i++){
            if( ((String) tableNames.get(i)).equalsIgnoreCase(toTest)){
                return true;
            }
        }
        return false;
    }

    /**
     * Executes the SQL command in the form of a String constructed by the other SQL command methods
     * @param cmd String representing a SQL command
     */
    public void executeCommand(String cmd){
        try{
            Class.forName("SQLiteJDBC");
            c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Brandon\\IdeaProjects\\JavaDatabase\\src\\SQLTest.db");
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            stmt.executeUpdate(cmd);
            stmt.close();
            c.close();
        }
        catch(Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Would you like to do another operation?(Y or N):");
        if(scnr.nextLine().equalsIgnoreCase("y")) mainMenu();
        else return;
    }

    public void executeFRCommand(String cmd){
        try{
            Class.forName("SQLiteJDBC");
            c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Brandon\\IdeaProjects\\JavaDatabase\\src\\ConsumerPriceIndex.db");

            stmt = c.createStatement();
            stmt.executeUpdate(cmd);
            stmt.close();
            c.close();
        }
        catch(Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }
}
