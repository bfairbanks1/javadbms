import java.util.*;
import java.io.*;

public class FileReader extends SQLiteJDBC {
    private String tableName;
    private String fileName;
    private String tempLine;

    public FileReader(String fileName, String tableName){
        this.tableName = tableName;
        this.fileName = fileName;
    }

    public void fillDatabase() {
        try {
            Scanner inFile = new Scanner(new FileInputStream((fileName + ".txt")), "ISO-8859-1");

            String insertCMD = "";
            String tableCMD;

            tempLine = inFile.nextLine();
            tableCMD = ("CREATE TABLE " + tableName + " (");
            tableCMD += (tempLine.substring(0, 9).trim() + " STRING, ");
            tableCMD += (tempLine.substring(10, 14).trim() + " INT, ");
            tableCMD += (tempLine.substring(15, 21).trim() + " STRING, ");
            tableCMD += (tempLine.substring(22, 27).trim() + " REAL)");
            tempLine.substring(28, tempLine.length());
            executeFRCommand(tableCMD);

            while (inFile.hasNextLine()) {
                tempLine = inFile.nextLine();
                insertCMD = "INSERT INTO " + tableName + " VALUES(";
                insertCMD += ("\'" + tempLine.substring(0, 13).trim() + "\', ");
                insertCMD += (tempLine.substring(14, 22).trim() + ", ");
                insertCMD += ("\'" + tempLine.substring(23, 30).trim() + "\', ");
                insertCMD += (tempLine.substring(31, 45).trim() + ")");
                //tempLine.substring(45, tempLine.length());
                executeFRCommand(insertCMD);
            }
        }
        catch(IOException e){
            System.out.println("Input file was not found or cannot be opened.");
            System.exit(0);
        }
        System.out.println("Table created and filled succesfully. " +
                "Goodbye!");
    }
}
