import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.ByteOrder;

public class DatabaseGUI{

    public DatabaseGUI(JDBC session, String toDisplay){
        JFrame dframe = new JFrame("Java DBMS");
        dframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dframe.setSize(400, 300);
        dframe.setLocationRelativeTo(null);

        //add message of previous actions upon return to menu
        JTextField message = new JTextField();
        if(!toDisplay.equals("")){
            message.setText(toDisplay);
            dframe.add(message, BorderLayout.NORTH);
        }

        String[] menuOptions = {"", "Create table", "Insert into table", "Drop table", "Query information"};

        final JPanel comboPanel = new JPanel();
        JLabel menuLbl = new JLabel("DB Options:");
        JComboBox menu = new JComboBox(menuOptions);

        comboPanel.add(menuLbl);
        comboPanel.add(menu);

        JButton menuBarBut = new JButton("SELECT");
        menuBarBut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox<String> temp = (JComboBox<String>) menu;
                String selectedChoice = (String) temp.getSelectedItem();
                if(selectedChoice.equals("Create table")){
                    new CreateTableGUI(session);
                    dframe.dispose();
                }
                else if(selectedChoice.equals("Insert into table")){
                    new InsertTableGUI(session);
                    dframe.dispose();
                }
                else if(selectedChoice.equals("Drop table")){
                    new DropTableGUI(session);
                    dframe.dispose();
                }
                else if(selectedChoice.equalsIgnoreCase("Query information")){
                    new QueryGUI(session);
                    dframe.dispose();
                }
            }
        });
        dframe.add(comboPanel, BorderLayout.WEST);
        dframe.add(menuBarBut, BorderLayout.EAST);
        dframe.setVisible(true);

    }
}
