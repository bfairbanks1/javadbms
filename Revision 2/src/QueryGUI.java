import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class QueryGUI {

    public QueryGUI(JDBC session){
        JFrame qframe = new JFrame("Java DBMS - Query");
        JLabel qlabel = new JLabel("Enter the Query you wish to make:");
        JTextField qfield = new JTextField("", 10);
        JButton qbutton = new JButton("QUERY");

        qframe.add(qlabel, BorderLayout.NORTH);
        qframe.add(qfield, BorderLayout.CENTER);
        qframe.add(qbutton, BorderLayout.SOUTH);
        qframe.pack();
        qframe.setLocationRelativeTo(null);
        qframe.setVisible(true);

        JButton exit = new JButton("EXIT");

        qbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!qfield.getText().equals("")){
                    /*This was to be the ScrollPane that the textArea would be added to, filled with the results
                    of the users SQL Query.
                    */
                    //JScrollPane sPane = new JScrollPane();
                    //JTextArea tArea = new JTextArea(session.executeQuery(gfield.getText()));

                    new DatabaseGUI(session, "Query functionality currently unavailable");
                    qframe.dispose();
                }
            }
        });
    }
}
