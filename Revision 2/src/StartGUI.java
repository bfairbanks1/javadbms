import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;

/**
 * This is the class that should be run to begin the program and begins by
 * selecting the database you would like to connect to.
 * Using prompts, textFields, and actionListeners on buttons
 * this program passes the user between JFrames to select and execute commands on their
 * selected database.
 */
public class StartGUI {
    public static void main(String args[]){
        new StartGUI();
    }

    public StartGUI(){
        JFrame start = new JFrame("Java DBMS");
        JPanel panel = new JPanel(new GridLayout(3, 1));
        start.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        start.setSize(600, 100);
        start.setLocationRelativeTo(null);

        JLabel title = new JLabel("Enter the name of the Database you would like to connect to:");
        JTextField usrIn = new JTextField("", 1);
        JButton button = new JButton("CONNECT");

        panel.add(title);
        panel.add(usrIn);
        panel.add(button);
        start.add(panel);
        start.setVisible(true);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDBC session = new JDBC((usrIn.getText() + ".db"));
                new DatabaseGUI(session, "");
                start.dispose();
            }
        });
    }
}
