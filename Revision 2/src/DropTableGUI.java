import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DropTableGUI {

    public DropTableGUI(JDBC session){
        JFrame dframe = new JFrame("Java DBMS - Drop Table");
        JLabel dlabel = new JLabel("Enter the name of the table you would like to delete:");
        JTextField dfield = new JTextField("",10);
        JButton dbutton = new JButton("DELETE");

        dframe.add(dlabel, BorderLayout.NORTH);
        dframe.add(dfield, BorderLayout.CENTER);
        dframe.add(dbutton, BorderLayout.SOUTH);
        dframe.pack();
        dframe.setLocationRelativeTo(null);
        dframe.setVisible(true);

        dbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String cmd = "DROP TABLE ";
                if(!dfield.getText().equals("")){
                    cmd += dfield.getText();
                    session.executeCommand(cmd);

                    new DatabaseGUI(session, "Table successfully deleted!");
                    dframe.dispose();
                }
            }
        });
    }
}
