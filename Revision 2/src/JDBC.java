import java.sql.*;
import java.util.ArrayList;

/**
 * @author Brandon Fairbanks
 * @version 12/10/18
 */
public class JDBC {
    private Statement stmt;
    private Connection c;
    private String url;

    public JDBC(String dbName) {
        try {
            Class.forName("JDBC");
            //This url will have to be modified for your local machine
            url = "jdbc:sqlite:C:\\Users\\Brandon\\IdeaProjects\\JavaDatabaseRev2\\src\\" + dbName;
            c = DriverManager.getConnection(url);
            System.out.println("Opened database successfully");
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.out.println("Could not connect, try again");
        }
    }

    public void executeCommand(String cmd){
        try{
            c = DriverManager.getConnection(url);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            stmt.executeUpdate(cmd);
            stmt.close();
            c.close();
        }
        catch(Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public ArrayList getTableNames(String tableName) {
        ArrayList<String> colNames = new ArrayList<>();
        try {
            c = DriverManager.getConnection(url);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM " + tableName);
            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();
            for (int i = 1; i <= columnCount; i++ ) {
                colNames.add(metaData.getColumnName(i));
            }
            c.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return colNames;
    }

    /**
     * This method requires far more work to implement and given time constrains I cannot
     * Conceptually I would add an additional prompt to the QueryGUI which would prompt for the table
     * name, then that would be passed to the getTableNames method. Once those were collected the rs.getString()
     * commands could be tailored to however many columns there are and their names. Finally, the Query
     * could be entered and run on the given table. I'm not certain how I would implement this part as
     * I would need to know from what the user was querying from to narrow the rs.getString() instances to only those
     * columns included in the query.
     */
    /*
    public String executeQuery(String cmd) {
        String result;
        try {
            c = DriverManager.getConnection(url);
            PreparedStatement stmt = c.prepareStatement(cmd);
            ResultSet rs;

            rs = stmt.executeQuery();
            while (rs.next()) {
                result = (rs.getString(1) + "\t" + rs.getString(2) + "\t" + rs.getString(3));
            }
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return result;
    }
    */
}
