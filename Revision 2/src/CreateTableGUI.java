import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreateTableGUI {

    private String cmd = "CREATE TABLE ";
    private String[] dataTypes = {"", "BOOLEAN", "INT", "DOUBLE", "STRING", "TEXT"};
    private String[] modifiers = {"", "PRIMARY KEY", "FOREIGN KEY", "NOT NULL"};

    public CreateTableGUI(JDBC session){

        JFrame cframe = new JFrame("Java DBMS - Create Table");
        cframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        cframe.setSize(600, 100);
        cframe.setLocationRelativeTo(null);

        JLabel tblNamePrmpt = new JLabel("Enter the of the new table:");
        JButton tblNameButton = new JButton("ENTER");
        JTextField tblNameField = new JTextField(1);

        cframe.add(tblNamePrmpt, BorderLayout.PAGE_START);
        cframe.add(tblNameField, BorderLayout.CENTER);
        cframe.add(tblNameButton, BorderLayout.SOUTH);

        cframe.setVisible(true);

        /**
         * Here are the options and the panel they will be added to
         */
        JPanel title = new JPanel(new GridLayout(1, 3));
        JPanel colOptions1 = new JPanel(new GridLayout(1, 3));
        JPanel colOptions2 = new JPanel(new GridLayout(1, 3));
        JPanel colOptions3 = new JPanel(new GridLayout(1, 3));
        JPanel colOptions4 = new JPanel(new GridLayout(1, 3));
        JPanel colOptions5 = new JPanel(new GridLayout(1, 3));

        JLabel ctitle = new JLabel("Column Name");
        JLabel dtitle = new JLabel("Data Type");
        JLabel mtitle = new JLabel("Modifiers");

        JTextField field1 = new JTextField(10);
        JTextField field2 = new JTextField(10);
        JTextField field3 = new JTextField(10);
        JTextField field4 = new JTextField(10);
        JTextField field5 = new JTextField(10);

        JComboBox dt1 = new JComboBox(dataTypes);
        JComboBox dt2 = new JComboBox(dataTypes);
        JComboBox dt3 = new JComboBox(dataTypes);
        JComboBox dt4 = new JComboBox(dataTypes);
        JComboBox dt5 = new JComboBox(dataTypes);

        JComboBox mod1 = new JComboBox(modifiers);
        JComboBox mod2 = new JComboBox(modifiers);
        JComboBox mod3 = new JComboBox(modifiers);
        JComboBox mod4 = new JComboBox(modifiers);
        JComboBox mod5 = new JComboBox(modifiers);

        title.add(ctitle);
        title.add(dtitle);
        title.add(mtitle);

        colOptions1.add(field1);
        colOptions1.add(dt1);
        colOptions1.add(mod1);

        colOptions2.add(field2);
        colOptions2.add(dt2);
        colOptions2.add(mod2);

        colOptions3.add(field3);
        colOptions3.add(dt3);
        colOptions3.add(mod3);

        colOptions4.add(field4);
        colOptions4.add(dt4);
        colOptions4.add(mod4);

        colOptions5.add(field5);
        colOptions5.add(dt5);
        colOptions5.add(mod5);

        JButton commitTable = new JButton("CREATE TABLE");

        /**
         * Here is where the table name will be accepted and the prompts/fields for
         * column name, data type, and modifiers will be swapped in
         */
        tblNameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!tblNameField.getText().equals("")){
                    cmd += tblNameField.getText();
                    cframe.remove(tblNamePrmpt);
                    cframe.remove(tblNameField);
                    cframe.remove(tblNameButton);
                    cframe.invalidate();

                    cframe.setLayout(new GridLayout(7, 1));
                    cframe.add(title);
                    cframe.add(colOptions1);
                    cframe.add(colOptions2);
                    cframe.add(colOptions3);
                    cframe.add(colOptions4);
                    cframe.add(colOptions5);
                    cframe.add(commitTable);
                    cframe.setSize(400, 300);
                    cframe.setLocationRelativeTo(null);

                    cframe.validate();
                }
            }
        });//tblButton ActionListener

        commitTable.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!field1.getText().equals("") && !dt1.getSelectedItem().equals("")){
                    cmd += (" (" + field1.getText() + " " + dt1.getSelectedItem() + " " + mod1.getSelectedItem());
                }
                if(!field2.getText().equals("") && !dt2.getSelectedItem().equals("")){
                    cmd += (", " + field2.getText() + " " + dt2.getSelectedItem() + " " + mod2.getSelectedItem());
                }
                if(!field3.getText().equals("") && !dt3.getSelectedItem().equals("")){
                    cmd += (", " + field3.getText() + " " + dt3.getSelectedItem() + " " + mod3.getSelectedItem());
                }
                if(!field4.getText().equals("") && !dt4.getSelectedItem().equals("")){
                    cmd += (", " + field4.getText() + " " + dt4.getSelectedItem() + " " + mod4.getSelectedItem());
                }
                if(!field5.getText().equals("") && !dt5.getSelectedItem().equals("")){
                    cmd += (", " + field5.getText() + " " + dt5.getSelectedItem() + " " + mod5.getSelectedItem());
                }
                cmd += ")";
                //System.out.println(cmd);
                session.executeCommand(cmd);
                new DatabaseGUI(session, "Table created successfully!");
                cframe.dispose();
            }
        });

    }//class end
}
