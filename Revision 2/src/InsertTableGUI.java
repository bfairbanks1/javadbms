import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Inet4Address;
import java.util.ArrayList;

public class InsertTableGUI {
    private int numCols;
    private ArrayList<String> cmdList = new ArrayList<>();
    private ArrayList<JPanel> fields = new ArrayList<>(5);
    private ArrayList<ArrayList<String>> fieldValues = new ArrayList<>();
    private String tableName;

    public InsertTableGUI(JDBC session){
        JFrame iframe = new JFrame("Java DBMS - Insert into table");
        iframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        iframe.setSize(400, 100);
        iframe.setLocationRelativeTo(null);

        JLabel title = new JLabel("Enter the name of the table you would like to insert into:");
        JTextField tblNameField = new JTextField("",10);
        JButton tblButton = new JButton("SELECT");

        iframe.add(title, BorderLayout.NORTH);
        iframe.add(tblNameField, BorderLayout.CENTER);
        iframe.add(tblButton, BorderLayout.SOUTH);
        iframe.setVisible(true);

        JButton insert = new JButton("INSERT");

        tblButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ArrayList<String> colNames = new ArrayList<>();
                if(!tblNameField.getText().equals("")){
                    tableName = tblNameField.getText();
                    colNames = session.getTableNames(tableName);

                    iframe.remove(title);
                    iframe.remove(tblNameField);
                    iframe.remove(tblButton);
                    iframe.invalidate();

                    numCols = colNames.size();
                    JPanel colNamePanel = new JPanel(new GridLayout(1, numCols));
                    //JPanel fields = new JPanel(new GridLayout(1, colNames.size()));
                    iframe.setLayout(new GridLayout(7, numCols));

                    for(int i = 1; i <= colNames.size(); i++){
                        colNamePanel.add(new JLabel(colNames.get(i - 1)));
                    }
                    iframe.add(colNamePanel);

                    fields.add(new JPanel(new GridLayout(1, numCols)));
                    fields.add(new JPanel(new GridLayout(1, numCols)));
                    fields.add(new JPanel(new GridLayout(1, numCols)));
                    fields.add(new JPanel(new GridLayout(1, numCols)));
                    fields.add(new JPanel(new GridLayout(1, numCols)));

                    for(int i = 1; i <= numCols; i++){
                        for(int j = 0; j < numCols; j++) {
                            fields.get(i - 1).add(new TextField(10));
                        }
                        iframe.add(fields.get(i - 1));
                    }
                    iframe.add(insert, BorderLayout.SOUTH);
                    iframe.setSize(300, 200);
                    iframe.pack();
                    iframe.setLocationRelativeTo(null);
                    iframe.validate();

                }
            }
        });

        insert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(int i = 0; i < numCols; i++){
                    ArrayList<String> temp = new ArrayList<>();
                    for(Component c : fields.get(i).getComponents()){
                        if(c instanceof JTextField){
                            temp.add(((JTextField)c).getText());
                        }
                        fieldValues.add(temp);
                    }
                }//outer for loop

                for(int i = 0; i < fieldValues.get(i).size(); i++){
                    String cmd = "INSERT INTO " + tableName + " VALUES (";
                    cmd += fieldValues.get(i).get(0);
                    for(int j = 1; j < fieldValues.get(i).size(); j++) {
                        //cmd += fieldValues.get(i).get(0);
                        cmd += (", " + fieldValues.get(i).get(j));
                    }
                    cmd += ")";
                    cmdList.add(cmd);
                    System.out.println(cmd);
                }

                for(int i = 0; i < cmdList.size(); i++){
                    session.executeCommand(cmdList.get(i));
                }

                new DatabaseGUI(session, "Values inserted successfully!");
                iframe.dispose();
            }//actionPerformed
        });//ActionListener
    }

}
